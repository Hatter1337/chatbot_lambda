Chatbot-Lambda
==============

> The **chatbot** directory is hidden because the author wants to leave the core code *private*.


The framework is designed to deploy a chatbot on [AWS Lambda](https://aws.amazon.com/lambda/).

The architecture is based on the principle of **Webhooks**; accordingly, only messengers supporting Webhooks can be added.

Python version: **3.7.4**

Supported messengers:
--------------
* Telegram

Architecture:
--------------
This library contains a working example with extensive architecture:

- Redis / DynamoDB (for data caching)
- PostgreSQL (for data storing)
- Resource: https://darksky.net
- S3 for images (optional)

> All this is in the directory "/components".
> Config: "/components/config/config.yml" - add the necessary data for connecting components.

> Do not forget to make changes to the "serverless.yml" and specify the messenger token.

Cheap way to use:
* DynamoDB (free tier) or EC2 t2.micro (free tier) with Redis ([link](https://medium.com/@feliperohdee/installing-redis-to-an-aws-ec2-machine-2e2c4c443b68)) & elastic IP
* RDS instance with public access (free tier)
* S3 public bucket (upload images from "/components/static/icons")

Safe way to use:
* Create VPC
* Add VPC to Lambda (use "serverless.yml")
* Create and configure NAT Gateway or NAT Instance
* Create ElasticCache-Redis or DynamoDB
* Use RDS private instance
* S3 public bucket (upload images from "/components/static/icons")