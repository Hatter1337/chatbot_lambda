import os
import json

from bot import configure_bot
from chatbot.log import logger
from chatbot.core.lambda_response import (
    LambdaResponse, SUCCESS_LAMBDA_RESPONSE, ERROR_LAMBDA_RESPONSE
)


def webhook(event, context):
    """Runs the Messenger webhook."""
    logger.info('Event.body: {}'.format(event.get('body')))

    if event.get('httpMethod') == 'POST' and event.get('body'):
        body = event['body']

        try:
            bot = configure_bot()
            json_body = json.loads(body)

            if json_body.get('entry'):
                bot.process(json_body['entry'][0])
            else:
                logger.error(f'WEBHOOK ERROR. Request has not body.entry.')

            return SUCCESS_LAMBDA_RESPONSE
        except AttributeError:
            logger.error('Can\'t start processing event.body')
        except Exception as error:
            logger.error(f'Bot processing error: {str(error)}')

    logger.error(f'WEBHOOK ERROR. Bad request. Event: {event}')
    return SUCCESS_LAMBDA_RESPONSE  # ERROR_LAMBDA_RESPONSE


def set_webhook(event, context):
    """Set the Messenger webhook."""

    verify_token = event['queryStringParameters'].get('hub.verify_token')
    challenge = int(event['queryStringParameters'].get('hub.challenge', 0))

    if verify_token == os.environ.get('VERIFY_TOKEN'):
        return LambdaResponse(
                status_code=200,
                body=challenge
            ).json()

    logger.error(f'SET WEBHOOK ERROR. Bad request. Event: {event}')
    return ERROR_LAMBDA_RESPONSE
