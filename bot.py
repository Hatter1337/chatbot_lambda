import os

from chatbot.log import logger
from chatbot.core.core import Chatbot
from components.storage.cache import DynamoCache
from chatbot.messengers.facebook.fabric import FacebookFabric
from chatbot.messengers.telegram.fabric import TelegramFabric
# Import blueprints:
from components.views.main import main_blueprint
from components.views.download import download_blueprint
from components.views.features import features_blueprint


def configure_bot():
    """Configure chat-bot."""

    messenger_name = os.environ.get('MESSENGER')

    if messenger_name == 'Telegram':
        messenger = TelegramFabric(
            token=os.environ.get('TOKEN')
        )
    elif messenger_name == 'Facebook':
        messenger = FacebookFabric(
            access_token=os.environ.get('ACCESS_TOKEN'),
            verify_token=os.environ.get('VERIFY_TOKEN')
        )
    else:
        logger.error('The specified messenger is not supported')
        return None

    bot = Chatbot(messenger=messenger, data_store=DynamoCache)
    # Provide blueprints:
    bot.register_blueprint(main_blueprint, default=True)
    bot.register_blueprint(download_blueprint)
    bot.register_blueprint(features_blueprint)

    return bot
