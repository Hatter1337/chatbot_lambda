from chatbot.core.response import Response
from chatbot.core.blueprint import Blueprint
from chatbot.core.endpoint import EndpointView
from chatbot.core.response_block.button import ButtonBlock
from chatbot.core.response_block.element import ElementBlock
from chatbot.core.response_block.generic import GenericBlock


class PushButtonEndpoint(EndpointView):
    """Example for button block."""

    def view(self, request):
        image_url = "https://i.dlpng.com/static/png/" \
                    "5409377-kek-png-92-images-in-collection-page-1-kek-png-960_600_preview.png"

        return Response(
            GenericBlock(
                elements=[
                    ElementBlock(
                        title="Make your choice:",
                        image_url=image_url,
                        buttons=[
                            ButtonBlock(title="Chocolate cake", data="/choice:chocolate"),
                            ButtonBlock(title="Cherry cake", data="/choice:cherry"),
                            ButtonBlock(title="Peach cake", data="/choice:peach")
                        ]
                    )
                ]
            )
        )


class CakeChoiceEndpoint(EndpointView):
    """Quick reply: user choice."""

    def view(self, request):
        text = request.text
        choice = text[text.find(":") + 1:] if ":" in text else "undefined"
        return f"You choose a {choice}."


features_blueprint = Blueprint("features", global_level=True)
features_blueprint.register_command(PushButtonEndpoint, command="/button")
features_blueprint.register_command(CakeChoiceEndpoint, command="/choice")
