from chatbot.log import logger
from chatbot.core.response import Response
from chatbot.core.redirect import redirect
from chatbot.core.blueprint import Blueprint
from chatbot.core.endpoint import EndpointView
from chatbot.core.response_block.text import TextBlock
from chatbot.core.response_block.button import ButtonBlock
from chatbot.core.response_block.element import ElementBlock
from chatbot.core.response_block.generic import GenericBlock
# components
from components.config import config
from components.storage.models import Users
from components.storage.db import session_scope
from components.helpers.coordinates import coordinates
from components.resources.dark_sky import DarkSkyResource

# Related views
from components.views.download import DownloadForecastEndpoint

KEYBOARD = {
    "weather": "/weather",
    "tomorrow forecast": "/forecast",
    "weather in city": "/city_name",
    "registration": "/start"
}
BOT_KEYBOARD = {
    "keyboard": [[key.capitalize()] for key in KEYBOARD.keys()],
    "one_time_keyboard": True
}


class HelpEndpoint(EndpointView):
    """Help endpoint."""

    def view(self, request):
        message = "This is a weather bot.\n" \
                  "Menu:\n" \
                  "- Weather - show current weather in your city;\n" \
                  "- Registration - create / change default name and city.\n" \
                  "- Help - show this message.\n\n" \
                  "Additional commands:" \
                  "- 'Tomorrow forecast' - show weather forecast in your city;' - \n" \
                  "- 'Weather in city' - enter the city where you want to know the weather;\n" \
                  "* The bot also generates a link to view the weather forecast in Google."

        return message


# /start (user connected to bot). Endpoints that the user passes for the first time:
class StartEndpoint(EndpointView):
    """Start (registration) - show information about the required input data for registration."""

    def view(self, request):
        message = "Enter your name and your city, separated by commas.\n" \
                  "Example: 'Alex, London'.\n" \
                  "Need help - /help."

        return Response(TextBlock(message), next_endpoint="main:registration")


class RegistrationEndpoint(EndpointView):
    """Registration - save user to DB."""

    def view(self, request):
        try:
            name, city = [value.strip() for value in request.text.split(",")]
        except (AttributeError, ValueError):
            return "Invalid input parameters"

        city_coordinates = coordinates(city)

        if city_coordinates:
            latitude, longitude = city_coordinates
            new_user = {
                "city": city.capitalize(),
                "latitude": latitude,
                "longitude": longitude,
                "messenger": request.messenger
            }

            request.session.update_data(new_user)

            # Save user in DB
            with session_scope() as session:
                new_user["user_id"] = str(request.user_id)
                new_user["name"] = name

                user = Users(**new_user)
                session.merge(user)
                session.commit()

                logger.info(f"Created new user: {request.user_id}")
                request.session.delete_state()  # Clear user state

            # Show the weather in the city by default
            return redirect("main:success_registration")

        return "Incorrect city name, the coordinates of the specified name is not found"


class SuccessRegistrationEndpoint(EndpointView):
    """Send success message and menu."""

    def view(self, request):
        message = "Congratulations!\n" \
                  "You have successfully registered and you can use the commands (in the menu) " \
                  "to view the weather and forecast. Enjoy :)"

        return message


class WeatherEndpoint(EndpointView):
    """Return weather forecast."""

    resource = DarkSkyResource

    def view(self, request):
        user_data = request.session.get_data()

        if request.resource_data:
            try:
                today = request.resource_data['currently']

                # City can be transferred in redirect data
                if request.redirect_data:
                    city = request.redirect_data.get('city', user_data.get('city'))
                else:
                    city = user_data.get('city')

                title = f"{today['summary']}"
                subtitle = f"City: {city}\n" \
                           f"Temperature: {today['temperature']} C\n" \
                           f"Humidity: {today['humidity']} %\n" \
                           f"Wind speed: {today['windSpeed']} m/s"

                image = f"{config['bucket']['host']}/{today['icon']}.png"

                google_weather = f"https://www.google.com/search?q=weather+{user_data.get('city')}"
                return Response(
                    GenericBlock(
                        elements=[
                            ElementBlock(
                                title=title,
                                subtitle=subtitle,
                                image_url=image,
                                buttons=[
                                    ButtonBlock(title="Tomorrow forecast", data="/forecast"),
                                    ButtonBlock(title="Google forecast", url=google_weather)
                                ]
                            )
                        ],
                        image_aspect_ratio='square'
                    )
                )

            except KeyError as error:
                logger.error(f'KeyError: {str(error)}')
                return "Sorry, the bot cannot display weather or forecast now :("

        return "Sorry, the weather service is overloaded :(\nTry later..."


# Recognition menu commands
class RecognitionEndpoint(EndpointView):
    """Recognition menu commands."""

    def view(self, request):
        user_message = request.text.lower()
        logger.info(f"RECOGNIZE: {user_message}")

        for key, value in KEYBOARD.items():
            if key == user_message:
                return redirect(f"main:{value}")
        return "Undefined command"


# Menu 1 ('Weather')
class WeatherRedirectEndpoint(EndpointView):
    """Redirect from command /weather to the weather endpoint."""

    def view(self, request):
        cache = request.session.get_data()

        if not (cache and all(key in cache for key in ("latitude", "longitude"))):
            with session_scope() as session:
                user = session.query(Users) \
                    .filter(Users.user_id == str(request.session.user_id)).first()

                if user:
                    user_data = {
                        "city": user.city,
                        "latitude": user.latitude,
                        "longitude": user.longitude
                    }
                    request.session.update_data(user_data)
                else:
                    return "Please register to use this command."

        return redirect("main:weather")


# Menu 2 ('Tomorrow forecast')
class ForecastRedirectEndpoint(EndpointView):
    """Redirect from command /forecast to the tomorrow forecast endpoint."""

    def view(self, request):
        cache = request.session.get_data()

        if not (cache and all(key in cache for key in ("latitude", "longitude"))):
            with session_scope() as session:
                user = session.query(Users) \
                    .filter(Users.user_id == str(request.session.user_id)).first()

                if user:
                    user_data = {
                        "city": user.city,
                        "latitude": user.latitude,
                        "longitude": user.longitude
                    }
                    request.session.update_data(user_data)
                else:
                    return "Please register to use this command."

        return redirect("main:forecast")


class ForecastEndpoint(EndpointView):
    """Weather forecast for tomorrow."""

    resource = DarkSkyResource

    def view(self, request):
        user_data = request.session.get_data()

        if request.resource_data:
            try:
                yesterday = request.resource_data['daily']['data'][2]

                image = f"{config['bucket']['host']}/{yesterday['icon']}.png"
                title = f"{yesterday['summary']}"
                subtitle = f"City: {user_data.get('city')}\n" \
                           f"Temperature: {yesterday['temperatureLow']} " \
                           f"/ {yesterday['temperatureHigh']} C\n" \
                           f"Humidity: {yesterday['humidity']} %\n" \
                           f"Wind speed: {yesterday['windSpeed']} m/s"
                google_weather = f"https://www.google.com/search?" \
                                 f"q=tomorrow+weather+{user_data.get('city')}"
                return Response(
                    GenericBlock(
                        elements=[
                            ElementBlock(
                                title=title,
                                subtitle=subtitle,
                                image_url=image,
                                buttons=[
                                    ButtonBlock(title="Weather", data="/weather"),
                                    ButtonBlock(title="Google forecast", url=google_weather)
                                ]
                            )
                        ],
                        image_aspect_ratio='square'
                    )
                )

            except KeyError as error:
                logger.error(f'KeyError: {str(error)}')
                return "Sorry, the bot cannot display weather or forecast now :("

        return "Sorry, the weather service is overloaded :(\nTry later..."


# Menu 3 ('Weather in city')
class CityNameEndpoint(EndpointView):
    """Show information about the required input data to show weather in the specified city."""

    def view(self, request):
        message = "Enter the name of the city where you want to know the weather.\n" \
                  "Example: 'London'"

        return Response(TextBlock(message), next_endpoint="main:weather_in_city")


class WeatherInCityEndpoint(EndpointView):
    """Get the name of the city; find the coordinates of the specified city."""

    def view(self, request):

        city_coordinates = coordinates(request.text)

        if city_coordinates:
            latitude, longitude = city_coordinates
            city_data = {
                "city": request.text.capitalize(),
                "latitude": latitude,
                "longitude": longitude
            }

            request.session.delete_state()  # Clear user state
            return redirect("main:weather", redirect_data=city_data)

        return "Incorrect city name, the coordinates of the specified city name is not found"


main_blueprint = Blueprint('main')
main_blueprint.register_command(StartEndpoint, command="/start")
main_blueprint.register_command(HelpEndpoint, command="/help")
main_blueprint.register_command(WeatherRedirectEndpoint, command="/weather")
main_blueprint.register_command(ForecastRedirectEndpoint, command="/forecast")
main_blueprint.register_command(CityNameEndpoint, command="/city_name")

main_blueprint.register_logic(RecognitionEndpoint, default=True)
main_blueprint.register_logic(WeatherEndpoint)
main_blueprint.register_logic(ForecastEndpoint)
main_blueprint.register_logic(RegistrationEndpoint)
main_blueprint.register_logic(WeatherInCityEndpoint)
main_blueprint.register_logic(SuccessRegistrationEndpoint)

# Related views
main_blueprint.register_command(DownloadForecastEndpoint, command="/download_forecast")
