import io
import json

from chatbot.core.response import Response
from chatbot.core.redirect import redirect
from chatbot.core.blueprint import Blueprint
from chatbot.core.endpoint import EndpointView
from chatbot.core.response_block.file import FileBlock
from components.resources.dark_sky import DarkSkyResource
from components.storage.models import Users
from components.storage.db import session_scope


class DownloadForecastEndpoint(EndpointView):
    """Save data about default city to session and redirect to "download_file"."""

    resource = DarkSkyResource

    def view(self, request):
        with session_scope() as session:
            user = session.query(Users) \
                .filter(Users.user_id == str(request.session.user_id)).first()

            if user:
                user_data = {
                    "city": user.city,
                    "latitude": user.latitude,
                    "longitude": user.longitude
                }
                request.session.update_data(user_data)

                return redirect("download:download_file")

            return "Please register to use this command."


class DownloadFileEndpoint(EndpointView):
    """Return file with weather forecast (for city in session)."""

    resource = DarkSkyResource

    def view(self, request):
        if request.resource_data:
            data = io.BytesIO(json.dumps(request.resource_data).encode())
            return Response(FileBlock("file.json", file_obj=data))

        return "Dark Sky API didn't return the response"


download_blueprint = Blueprint("download")
download_blueprint.register_logic(DownloadFileEndpoint)
