import os
import yaml


filename = os.path.join(os.path.dirname(__file__), "config.yml")
with open(filename) as yml_file:
    config = yaml.load(yml_file, Loader=yaml.FullLoader)
