from geopy import geocoders

GN_USERNAME = "hatter1337"


def coordinates(city):
    gn = geocoders.GeoNames(username=GN_USERNAME)

    if city:
        location = gn.geocode(city)
        if location:
            return location.latitude, location.longitude
