from contextlib import contextmanager

from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy_utils import database_exists, create_database, drop_database

from chatbot.log import logger
from components.config import config
from components.storage.models.base import Base


DATABASE_URL = "postgresql+psycopg2://{user}:{password}@{host}:{port}/{name}".format(
    user=config["db"]["user"],
    password=config["db"]["password"],
    host=config["db"]["host"],
    port=config["db"]["port"],
    name=config["db"]["name"]
)


options = {
    "pool_recycle": 3600,
    "pool_size": 20,
    "pool_timeout": 60,
    "max_overflow": 60,
    "echo": config["db"]["echo"]
}
engine = create_engine(DATABASE_URL, **options)

Session = sessionmaker()
Session.configure(bind=engine)


@contextmanager
def session_scope(expire=True):
    """
    Provide a transactional scope around a series of operations.

    Args:
        expire: Defaults to True.
        When True, all instances will be fully expired after each commit().

    """
    session = Session(expire_on_commit=expire)
    try:
        yield session
        session.commit()
    except Exception:
        session.rollback()
        raise
    finally:
        session.close()


def recreate_db():
    logger.info("Recreating database ...")
    if database_exists(DATABASE_URL):
        drop_database(DATABASE_URL)
        engine.dispose()
        create_database(DATABASE_URL)
    else:
        create_database(DATABASE_URL)
    Base.metadata.create_all(engine)
    logger.info("Created new database")


if __name__ == "__main__":
    # recreate_db()

    # add models to /models/__init__.py
    Base.metadata.create_all(engine)
