from sqlalchemy import Column, String, Float

from components.storage.models.base import Base


class Users(Base):
    """ Model: Users. """

    __tablename__ = "users"

    user_id = Column(String(20), primary_key=True)
    name = Column(String(100))
    city = Column(String(100))
    latitude = Column(Float)
    longitude = Column(Float)
    messenger = Column(String(20))
