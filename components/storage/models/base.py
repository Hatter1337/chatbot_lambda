from sqlalchemy.ext.declarative import declarative_base


class BaseModel:
    """ Model: Base. """

    def __init__(self, **kwargs):
        for key, value in kwargs.items():
            setattr(self, key, value)

    def __repr__(self):
        values = vars(self)

        return "<%s(" % str(__class__.__name__) + ", ".join([
            str(values[attr]) for attr in self.__mapper__.columns.keys()
            if attr in values
        ]) + ")>"


Base = declarative_base(cls=BaseModel)
