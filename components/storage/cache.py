import json
import time

import redis
import boto3

from chatbot.log import logger
from components.config import config
from chatbot.core.abstracts.data_store import DataStore


class RedisCache(DataStore):
    """Cache JSON-data to Redis."""

    client = None

    def __new__(cls, *args, **kwargs):
        if not cls.client:
            cls.client = redis.Redis(
                host=config['redis']['host'],
                port=config['redis']['port'],
                password=config['redis']['password']
            )
        return object.__new__(cls)

    def set(self, key, value):
        """Redis - SET."""

        try:
            json_value = json.dumps(value)
            logger.info(f'REDIS. SET "{key}": {json_value}')
            self.client.set(key, json_value, ex=config['redis'].get('expire', 21600))
        except Exception as error:
            logger.error(f'REDIS-SET ERROR:{str(error)}')

    def get(self, key):
        """Redis - GET."""

        try:
            value = self.client.get(key)
            if not value:
                logger.info(f'REDIS. GET "{key}": null')
                return None

            json_value = json.loads(value)
            logger.info(f'REDIS. GET "{key}": {json_value}')
            return json_value
        except json.decoder.JSONDecodeError:
            logger.error(f'REDIS DECODE ERROR key: "{key}"')
        except Exception as error:
            logger.error(f'REDIS-GET ERROR:{str(error)}')

    def delete(self, key):
        """Redis - DELETE."""

        try:
            logger.info(f'REDIS. DELETE "{key}"')
            self.client.delete(key)
        except Exception as error:
            logger.error(f'REDIS-DELETE ERROR:{str(error)}')

    def get_user(self):
        """Get user."""

        return self.get(self.user_id) or {}

    def delete_user(self):
        """Delete user."""

        self.delete(self.user_id)

    def get_state(self):
        """Get user state."""

        return self.user.get('state')

    def get_data(self):
        """Get user data."""

        return self.user.get('data')

    def set_state(self, state):
        """Set user state (complete rewriting the user["state"])."""

        if self.user:
            self.user.update({'state': state})
        else:
            self.user['state'] = state
        self.set(self.user_id, self.user)

    def delete_state(self):
        """Delete user state."""

        if self.user and 'state' in self.user:
            del self.user['state']
            self.set(self.user_id, self.user)

    def set_data(self, data):
        """Set user data (complete rewriting the dictionary user["data"])."""

        if self.user:
            self.user.update({'data': data})
        else:
            self.user['data'] = data
        self.set(self.user_id, self.user)

    def update_data(self, sub_data):
        """Update user data (partial change dictionary user["data"])."""

        if self.user and self.user.get('data'):
            self.user['data'].update(sub_data)
        else:
            self.user['data'] = sub_data
        self.set(self.user_id, self.user)


class DynamoCache(DataStore):
    """Cache JSON-data to DynamoDB."""

    client = None

    def __new__(cls, *args, **kwargs):
        if not cls.client:
            cls.client = boto3.client('dynamodb')
        return object.__new__(cls)

    @staticmethod
    def ttl():
        return str(int(time.time() + config['dynamodb'].get('ttl', 21600)))

    def set(self, key, value):
        """DynamoDB - SET."""

        try:
            json_value = json.dumps(value)
            logger.info(f'DynamoDB. SET "{key}": {json_value}')

            self.client.put_item(
                TableName=config['dynamodb']['table'],
                Item={
                    'key': {'S': str(key)},
                    'value': {'S': json_value},
                    'ttl': {'N': self.ttl()}
                }
            )
        except Exception as error:
            logger.error(f'DynamoDB-SET ERROR:{str(error)}')

    def get(self, key):
        """DynamoDB - GET."""

        try:
            response = self.client.get_item(
                TableName=config['dynamodb']['table'],
                Key={'key': {'S': str(key)}}
            )
            value = response.get('Item', {}).get('value', {}).get('S')

            if not value:
                logger.info(f'DynamoDB. GET "{key}": null')
                return None

            json_value = json.loads(value)
            logger.info(f'DynamoDB. GET "{key}": {json_value}')
            return json_value
        except json.decoder.JSONDecodeError:
            logger.error(f'DynamoDB DECODE ERROR key: "{key}"')
        except Exception as error:
            logger.error(f'DynamoDB-GET ERROR:{str(error)}')

    def delete(self, key):
        """DynamoDB - DELETE."""

        try:
            logger.info(f'DynamoDB. DELETE "{key}"')
            self.client.delete_item(
                TableName=config['dynamodb']['table'],
                Key={'key': {'S': str(key)}}
            )
        except Exception as error:
            logger.error(f'DynamoDB-DELETE ERROR:{str(error)}')

    def get_user(self):
        """Get user."""

        return self.get(self.user_id) or {}

    def delete_user(self):
        """Delete user."""

        self.delete(self.user_id)

    def get_state(self):
        """Get user state."""

        return self.user.get('state')

    def get_data(self):
        """Get user data."""

        return self.user.get('data')

    def set_state(self, state):
        """Set user state (complete rewriting the user["state"])."""

        if self.user:
            self.user.update({'state': state})
        else:
            self.user['state'] = state
        self.set(self.user_id, self.user)

    def delete_state(self):
        """Delete user state."""

        if self.user and 'state' in self.user:
            del self.user['state']
            self.set(self.user_id, self.user)

    def set_data(self, data):
        """Set user data (complete rewriting the dictionary user["data"])."""

        if self.user:
            self.user.update({'data': data})
        else:
            self.user['data'] = data
        self.set(self.user_id, self.user)

    def update_data(self, sub_data):
        """Update user data (partial change dictionary user["data"])."""

        if self.user and self.user.get('data'):
            self.user['data'].update(sub_data)
        else:
            self.user['data'] = sub_data
        self.set(self.user_id, self.user)
