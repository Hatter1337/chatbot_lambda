import requests

from components.config import config
from chatbot.core.resource import AbstractResource


class DarkSkyResource(AbstractResource):
    """API doc: https://darksky.net/dev/docs. Alternative: use forecastiopy3 lib."""

    def __init__(self):
        self.api_url = config["resource"]["dark_sky_url"]
        self.api_key = config["resource"]["dark_sky_key"]

    def receive(self, request):
        if request.redirect_data:
            user_data = request.redirect_data
        elif request.session:
            user_data = request.session.get_data()
        else:
            user_data = None

        if user_data and all(key in user_data for key in ("latitude", "longitude")):
            with requests.Session() as session:
                url = self.api_url.format(
                    api_key=self.api_key,
                    latitude=user_data["latitude"],
                    longitude=user_data["longitude"]
                )
                response = session.get(url)

                if response.status_code == 200:
                    return response.json()
