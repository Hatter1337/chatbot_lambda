import os
import yaml


# Quick way to upload files in Telegram - https://core.telegram.org/bots/api#sending-files
telegram_files_config = os.path.join(os.path.dirname(__file__), "telegram_file_ids.yml")
with open(telegram_files_config) as yml_file:
    telegram_files = yaml.load(yml_file, Loader=yaml.FullLoader)
